from middlewares.error_handler import ErrorHandler
from fastapi import FastAPI
from routers.libro import libros_router
from routers.categoria import categorias_router
from config.database import engine, Base
from fastapi.middleware.cors import CORSMiddleware
from middlewares import cors


Base.metadata.create_all(bind=engine)
app = FastAPI()
app.title = "Libreria Project First Test Jair"
app.version = "0.0.1"

# Registra los routers
app.add_middleware(ErrorHandler) 
#app.include_router(user_router)
app.include_router(libros_router)
app.include_router(categorias_router)
cors.add_cors_middleware(app)



