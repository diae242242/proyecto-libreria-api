from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship
from config.database import Base

from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship
from config.database import Base

class Categoria(Base):
    __tablename__ = 'categorias'

    id = Column(Integer, primary_key=True, index=True)
    nombre = Column(String, unique=True, index=True)

    libros = relationship("Libro", back_populates="categoria")
