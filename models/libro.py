from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship
from config.database import Base
from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base





class Libro(Base):
    __tablename__ = 'libros'

    codigo = Column(Integer, primary_key=True, index=True, autoincrement=True)
    titulo = Column(String, index=True)
    autor = Column(String)
    año = Column(Integer)
    cant_paginas = Column(Integer)
    categoria_id = Column(Integer, ForeignKey("categorias.id"))

    categoria = relationship("Categoria", back_populates="libros")
