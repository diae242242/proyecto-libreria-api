import os
from sqlalchemy import create_engine
from sqlalchemy.orm.session import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

# Nombre del archivo de la base de datos
sqlite_file_name = "database.sqlite"

# Directorio actual del script
base_dir = os.path.dirname(os.path.abspath(__file__))

# Ruta absoluta del archivo de la base de datos
database_path = os.path.join(base_dir, sqlite_file_name)

# URL de la base de datos
database_url = f"sqlite:///{database_path}"

# Imprimir la URL de la base de datos para verificarla
print(database_url)

# Crear el motor de la base de datos
engine = create_engine(database_url, echo=True)
Session = sessionmaker(bind=engine)
Base = declarative_base()
