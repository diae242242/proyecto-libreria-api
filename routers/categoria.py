from fastapi import APIRouter, Depends, HTTPException
from fastapi.responses import JSONResponse
from models.libro import Libro as LibroModel
from models.categoria import Categoria as CategoriaModel
from config.database import Session
from fastapi import FastAPI, Body, Path, Query, Request, HTTPException, Depends
from fastapi.responses import HTMLResponse, JSONResponse
from fastapi.security.http import HTTPAuthorizationCredentials
from pydantic import BaseModel, Field
from typing import Coroutine, Optional, List, Set
from fastapi.security import HTTPBearer
from config.database import Session, engine, Base
from models.libro import Libro as LibroModel
from models.categoria import Categoria as CategoriaModel



categorias_router = APIRouter()


@categorias_router.get('/categorias', tags=['categoria'])
def get_categorias():
    db = Session()
    categorias = db.query(CategoriaModel).all()
    db.close()
    return categorias

@categorias_router.delete('/categoria/{categoria_nombre}', tags=['categoria'])
def delete_categoria(categoria_nombre: str):
    db = Session()
    categoria = db.query(CategoriaModel).filter(
        CategoriaModel.nombre == categoria_nombre).first()
    if not categoria:
        db.close()
        return JSONResponse(content={"message": "La categoria no existe"})
    libros = db.query(LibroModel).filter(
        LibroModel.categoria == categoria).all()  
    if libros:
        db.close()
        return JSONResponse(content={"message": "La categoria está asociada a libros y no puede ser eliminada"})
    db.delete(categoria)
    db.commit()
    db.close()
    return JSONResponse(content={"message": "Se ha eliminado la categoria"})

@categorias_router.put('/categoria/{categoria_nombre}', tags=['categoria'])
def update_categoria(categoria_nombre: str, new_nombre: str):
    db = Session()
    categoria = db.query(CategoriaModel).filter(
        CategoriaModel.nombre == categoria_nombre).first()
    if not categoria:
        db.close()
        return JSONResponse(content={"message": "La categoria no existe"})
    categoria.nombre = new_nombre
    db.commit()
    db.close()
    return JSONResponse(content={"message": "Se ha modificado la categoria"})


@categorias_router.post('/categoria/', tags=['categoria'])
def create_categoria(categoria_nombre: str):
    db = Session()
    categoria = CategoriaModel(nombre=categoria_nombre)
    db.add(categoria)
    db.commit()
    db.close()
    return JSONResponse(content={"message": "Se ha registrado la categoria"})