from fastapi import APIRouter, Depends, HTTPException
from fastapi.responses import JSONResponse
from models.libro import Libro as LibroModel
from models.categoria import Categoria as CategoriaModel
from config.database import Session
from fastapi import FastAPI, Body, Path, Query, Request, HTTPException, Depends
from fastapi.responses import HTMLResponse, JSONResponse
from fastapi.security.http import HTTPAuthorizationCredentials
from pydantic import BaseModel, Field
from typing import Coroutine, Optional, List, Set
from fastapi.security import HTTPBearer
from config.database import Session, engine, Base
from models.libro import Libro as LibroModel
from models.categoria import Categoria as CategoriaModel


libros_router = APIRouter()




@libros_router.get('/', tags=["Home"])
def message():
    return HTMLResponse('<h1> Hello World </h1>')

libro = [
    
]
class Libro(BaseModel):
    codigo: int
    titulo: str = Field(min_length=1, max_length=30)
    autor: str = Field(min_length=1, max_length=50)
    año: int
    categoria: str = Field(min_length=1, max_length=30)
    cant_paginas: int = Field(ge=1, le=2000)

    class Config:
        json_schema_extra = {
            "example": {
                "codigo": 1,
                "titulo": "Recuerdos",
                "autor": "Juan",
                "año": 2020,
                "categoria": "aventura",
                "cant_paginas": 10
            }
        }

@libros_router.get('/libros', tags=['libros'])
def get_libros():
    db = Session()
    libros = db.query(LibroModel).all()
    libros_info = [
        {
            "codigo": libro.codigo,
            "titulo": libro.titulo,
            "autor": libro.autor,
            "año": libro.año,
            "categoria": libro.categoria.nombre if libro.categoria else None,
            "cant_paginas": libro.cant_paginas
        }
        for libro in libros
    ]
    db.close()
    return libros_info


@libros_router.post('/libro/', tags=['libro'])
def create_libro(libro: Libro):
    db = Session()
    categoria = db.query(CategoriaModel).filter(
        CategoriaModel.nombre == libro.categoria).first()
    if not categoria:
        db.close()
        return JSONResponse(content={"message": "La categoría no existe"})
    new_libro = LibroModel(
        titulo=libro.titulo,
        autor=libro.autor,
        año=libro.año,
        categoria=categoria,  
        cant_paginas=libro.cant_paginas
    )
    db.add(new_libro)
    db.commit()
    db.close()
    return JSONResponse(content={"message": "Se ha registrado el libro"})





@libros_router.put('/libro/{codigo}', tags=['libro'])
def update_libro(codigo: int, libro: Libro):
    db = Session()
    libro_db = db.query(LibroModel).filter(LibroModel.codigo == codigo).first()
    if not libro_db:
        db.close()
        raise HTTPException(status_code=404, detail="El libro no existe")
    
    categoria_db = db.query(CategoriaModel).filter(CategoriaModel.nombre == libro.categoria).first()
    if not categoria_db:
        db.close()
        raise HTTPException(status_code=404, detail="La categoría no existe")
    
    # Actualizamos los atributos del libro con los valores proporcionados
    libro_db.titulo = libro.titulo
    libro_db.autor = libro.autor
    libro_db.año = libro.año
    
    # Asignamos la nueva categoría al libro
    libro_db.categoria = categoria_db
    
    libro_db.cant_paginas = libro.cant_paginas
    db.commit()
    db.close()
    
    return {"message": "Se ha modificado el libro"}



@libros_router.delete('/libro/{codigo}', tags=['libro'])
def delete_libro(codigo: int):
    db = Session()
    libro = db.query(LibroModel).filter(LibroModel.codigo == codigo).first()
    if not libro:
        db.close()
        return JSONResponse(content={"message": "El libro no existe"})
    db.delete(libro)
    db.commit()
    db.close()
    return JSONResponse(content={"message": "Se ha eliminado el libro"})


@libros_router.get('/libro/{codigo}', tags=['libro'])
def get_libro(codigo: int):
    db = Session()
    libro = db.query(LibroModel).filter(
        LibroModel.codigo == codigo).first()
    libro_info = {
        "codigo": libro.codigo,
        "titulo": libro.titulo,
        "autor": libro.autor,
        "año": libro.año,
        "categoria": libro.categoria.nombre, 
        "cant_paginas": libro.cant_paginas
    }
    db.close()
    return libro


from fastapi import HTTPException

@libros_router.get('/libro/categoria/{categoria}', tags=['libro'])
def get_libro_by_categoria(categoria: str):
    db = Session()
    categoria_db = db.query(CategoriaModel).filter(CategoriaModel.nombre == categoria).first()
    if not categoria_db:
        db.close()
        raise HTTPException(status_code=404, detail="La categoría no existe")
    
    libros = categoria_db.libros
    db.close()
    
    if not libros:
        raise HTTPException(status_code=404, detail="No se encontraron libros para esta categoría")
    
    libros_info = []
    for libro in libros:
        libro_dict = {
            "codigo": libro.codigo,
            "titulo": libro.titulo,
            "autor": libro.autor,
            "año": libro.año,
            "categoria": categoria_db.nombre,  # Utilizamos el nombre de la categoría en lugar de su ID
            "cant_paginas": libro.cant_paginas
        }
        libros_info.append(libro_dict)
    
    return libros_info





